import './App.css';
import Navbar from './components/AppNavbar';
import Footer from './components/Footer';
import Home from './pages/Home';
import Register from './pages/Registration';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Product from './pages/Product';
import ProductView from './pages/SingleProduct';
import Admin from './pages/AdminDashboard';

import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';

import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';

function App() {


  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })


  const unsetUser = () => {
    localStorage.clear();
    setUser({
      id: null,
      isAdmin: null
    })
  }


  useEffect(() => {
      fetch(`https://sheltered-mesa-34175.herokuapp.com/users/details`, {
        headers: {
            Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
        }
      })
      .then(result => result.json())
      .then(convertedResult => {
          if (convertedResult._id !== "undefined") {
            setUser({
                id: convertedResult._id,
                isAdmin: convertedResult.isAdmin
            })
          } else {
            setUser({
                id: null,
                isAdmin: null
            })
          }
      })
  },[])


  return (

    <UserProvider value={{user, unsetUser, setUser}}>
      <Router>
          <Navbar />
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/products" component={Product} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/logout" component={Logout} />
                <Route exact path="/products/:productId" component ={ProductView} />
                <Route exact path="/admin-dashboard/" component={Admin} />
            </Switch>
            <Footer/>
      </Router>
    </UserProvider>

  );
}

export default App;
