import { useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import UserContext from '../UserContext';

export default function AppNavbar() {

    const { user } = useContext(UserContext);

    return (
        <Navbar expand="lg">
            <Container>
                <Navbar.Brand>
                    <Nav.Link as={NavLink} to="/" ><img src="/logo.png" alt="logo" /></Nav.Link>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />

                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto">
                        <Nav.Link as={NavLink} className="default-anchor x-pad-right" to="/">Home</Nav.Link> 
                        <a className="default-anchor x-pad-right nav-link" href="https://juandersquad.com/" rel="noreferrer" target="_blank">Blogs</a> 
                        <Nav.Link as={NavLink} className="default-anchor" to="/products" >Products</Nav.Link> 
                    
                        {(user.id) ?    
                            <Nav.Link as={NavLink} className="login" to="/logout" >Logout</Nav.Link> :
                            <>
                                <Nav.Link as={NavLink} to="/login" className="login">Login</Nav.Link>
                                <Nav.Link as={NavLink} to="/register" className="join-now">Join Now</Nav.Link>
                            </> 
                        }   

 
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}