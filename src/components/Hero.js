import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col'
import { Link } from 'react-router-dom';

function Hero({data}) {
    
    // object destructuring
    const { title, tagLine } = data;

    return (
        <section id="hero-banner">
            <Row className='align-items-center'>
                <Col className="p-5">
                    <h1 className='fredoka-font'>{title}</h1>
                    <p>{tagLine}</p>
                    <Link to="/products" className="shop-cta">Shop Now</Link>
                </Col>

                <Col className="text-center">
                    <img src="./HeroBag.png" alt="hero bag"/>
                </Col>
            </Row>
        </section>
    )
}


export default Hero; 