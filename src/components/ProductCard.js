import Card from 'react-bootstrap/Card';
import {Link} from 'react-router-dom';

export default function ProductCard({productInfo}) {
    

    const{_id, name, description, price, images, isActive} = productInfo;
   
    return (
      
        <Card>
            <Card.Body>
                <div className="flex-in">
                    <div className='image-thumbnail'>
                        <img src={images} alt="product thumbnail"/>
                    </div>
                    <div className='product-details'>
                        <Card.Title> {name} </Card.Title>
                        <Card.Subtitle>Product Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                            {
                                isActive ? 
                                <Card.Text>Status: Available</Card.Text> : 
                                <Card.Text>Status: Unavailable</Card.Text>
                            }
                        <Card.Text className="product-price">&#8369; {price}</Card.Text>
                        <Link className="view-product" to={`/products/${_id}`}> View Product </Link>
                    </div>
                </div>
            </Card.Body>
        </Card>
    )

}