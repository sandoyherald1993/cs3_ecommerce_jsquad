import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'


function Footer() {
  return (
    <footer className="absolute-bottom pb-1 pt-2 ">
        <Container>
            <Row>
                <Col>JuanderSquad E-commerce</Col>
                <Col className="text-right">Zuitt Coding Bootcamp © 2021</Col>
            </Row>
        </Container>
    </footer>
  );
}

export default Footer;
