import Container from 'react-bootstrap/Container';
import Hero from '../components/Hero';
import ProductCard from "../components/ProductCard";
import {useEffect ,useState} from 'react';



export default function Home() {
    let info = {
        title: "We offer best backpacks for \n your wonderful travels",
        tagLine: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, \n sed do eiusmod tempor incididunt ut labore "
    }

    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch("https://sheltered-mesa-34175.herokuapp.com/products/onstock/highlight").then(fetchOutcome => fetchOutcome.json()).then(convertedData => {

            setProducts(convertedData.map(product => {
                return (
                    <ProductCard key={product._id} productInfo={product}/>
                )
            }))
        })
   
    })


    return (
        <div className="main">
            <Container>
                <Hero data={info}/>

                <section id="product-highlights">
                    <h3 className='fredoka-font'>Our Latest Products</h3>
                    <div className="product-list">
                        {products}
                    </div>
                </section>
                
            </Container>
        </div>
    )
}