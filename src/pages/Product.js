import ProductCard from "../components/ProductCard";
import {Container, Form, Button} from 'react-bootstrap';
import {useEffect ,useState} from 'react';


export default function Products() {
    const [products, setProducts] = useState([]);


    useEffect(() => {
        fetch("https://sheltered-mesa-34175.herokuapp.com/products/onstock").then(fetchOutcome => fetchOutcome.json()).then(convertedData => {

            setProducts(convertedData.map(product => {
                return (
                    <ProductCard key={product._id} productInfo={product}/>
                )
            }))
        })
   
    })

    return (
        <Container>
            <div className="search-form">
                <Form>
                    <div className="search-wrapper">
                        <Form.Group controlId="searchProduct">
                            <Form.Control type="text" placeholder="Search Product"  required/>
                        </Form.Group>
                        <Button type="submit" id="submitBtn" className="default-cta" >Search</Button>
                    </div>

                </Form>
            </div>
            <div className="product-list">
                {products}
            </div>
        </Container>
    )
}