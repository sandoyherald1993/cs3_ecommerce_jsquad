import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useState , useContext, useEffect} from 'react';
import UserContext from '../UserContext';
import { Link , useParams} from 'react-router-dom';

 export default function ProductView() {
    const [name, setName] = useState("");
	const [description, setDescription] = useState("");
    const [image, setImage] = useState("");
	const [price, setPrice] = useState(0);

    const { user } = useContext(UserContext);
    const { productId } = useParams();



    useEffect(()=> {

		fetch(`https://sheltered-mesa-34175.herokuapp.com/products/search/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setImage(data.images);
            setPrice(data.price);
            

		});

	}, [productId]);

    return (
      <Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body className="text-center">
                            <div className='image-thumbnail'>
                                <img src={image} alt="product thumbnail"/>
                            </div>

                            <Card.Title> {name} </Card.Title>
                            <Card.Subtitle>Product Description</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Text className="product-price">&#8369; {price}</Card.Text>
                        
                            { (user.id) ? 
                                    <Button variant="primary">Add to Cart</Button>
                                        : 
                                    <Link className="btn btn-danger btn-block" to="/login">Log in to Buy</Link>
                            }
            
                    
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
 }

