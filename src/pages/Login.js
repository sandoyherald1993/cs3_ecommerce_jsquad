import { Form, Button, Container } from 'react-bootstrap';
import { useState,useEffect, useContext } from 'react';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Redirect} from "react-router-dom";

function Login() {

    const {user, setUser} = useContext(UserContext)

    const [email,setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);


    const authenticate = (e) => {
        e.preventDefault();


        const retrieveUserDetails = (token) => {
            fetch(`https://sheltered-mesa-34175.herokuapp.com/users/details`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(result => result.json())
            .then(convertedResult => {
                
                setUser({
                    id: convertedResult._id,
                    isAdmin: convertedResult.isAdmin
                })

            })
        }

        fetch('https://sheltered-mesa-34175.herokuapp.com/users/login', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password,
            })
        })
        .then(res => res.json())
        .then(data => {

            if (typeof data.accessToken !== "undefined") {

                localStorage.setItem('accessToken', data.accessToken);

                retrieveUserDetails(data.accessToken);
                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to the App!"
                })
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Check your login details and try again!"
                })
            }
        })
    }

    useEffect(()=> {
        if(email !== "" && password !== "") {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email,password]);

    return (

        (user.id) ?  
        <Redirect to="/products"/> : 
        <div className="main login-view">
            <Container>
                <div className="login-header">
                      <h1 className="fredoka-font pb-4 pt-5 text-center">Create your account here!</h1>
                </div>
              
                <Form onSubmit={e => authenticate(e)}>
                    <Form.Group controlId="userEmail"  value={email} onChange={event => setEmail(event.target.value)}>
                        <Form.Label> Email Address: </Form.Label>
                        <Form.Control type="email" placeholder="Insert Email Here" required autoComplete='false'/>
                    </Form.Group>

                    <Form.Group controlId="userPassword" value={password} onChange={event => setPassword(event.target.value)}>
                        <Form.Label> Password: </Form.Label>
                        <Form.Control type="password" placeholder="Insert Password Here" required autoComplete='false'/>
                    </Form.Group>

                    {
                        isActive ? 
                        <Button type="submit" id="submitBtn" variant="success" className="btn btn-block">Login</Button> : 
                        <Button type="submit" id="submitBtn" variant="danger" className="btn btn-block" disabled>Login</Button>
                    }
                </Form>
            </Container>
        </div>
    )
}

export default Login;