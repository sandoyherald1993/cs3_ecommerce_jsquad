import { Form, Button, Container} from 'react-bootstrap';
import { useState, useEffect , useCallback} from 'react';
import { useHistory } from 'react-router-dom';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col'
import Swal from 'sweetalert2';


export default function Register() {
    const history = useHistory();
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [middleName, setMiddleName] = useState('');
    const [email, setEmail] = useState('');
    const [password,setPassword] = useState('');
    const [confirmPass,setConfirmPass] = useState('');
    const [mobileNo, setMobileNo] = useState('');


    const [network, setNetwork] = useState('');
    const [numberStatus, setNumberStatus] = useState('Please provide a valid 11 digit number!');
    
    const [ isRegisterBtnActive, setRegisterBtnActive] = useState('');    
    const [isMatched, setIsMatched] = useState(false);
    const [isNumberValid, setIsNumberValid] = useState(false);


    let networkGlobe = "Globe";
    let networkSmart = "Smart";

    function handleSubmit(e) {
        e.preventDefault();

        fetch('https://sheltered-mesa-34175.herokuapp.com/users/register', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                middleName: middleName,     
                lastName: lastName,
                email: email,
                password: password,
                mobileNo: mobileNo
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                Swal.fire({
                    title: `Hi ${firstName} account is registered successfully!`,
                    icon: "success",
                    text: `Welcome to Juandersquad!`,
                });            
                
                history.push('/login');
            } else {
                Swal.fire({
                    title: `Email already exist`,
                    icon: "error",
                    text: `Please try another email`,
                });              
            }
        })
    }

    const mobileNumberCheck = useCallback(() => {
        let networkCheck = mobileNo.slice(0,4);
            
        if (mobileNo.length === 11) {
            if(networkCheck === '0995' || networkCheck === '0917') {
                setNetwork(networkGlobe);
                setIsNumberValid(true);
                setRegisterBtnActive(true);
            } else if(networkCheck === '0998' || networkCheck === '0908') {
                setNetwork(networkSmart);
                setIsNumberValid(true);
                setRegisterBtnActive(true);
            } else {
                setNumberStatus('Network Invalid');
                setIsNumberValid(false);
                setRegisterBtnActive(false);
            }
        } else {
            setIsNumberValid(false);
            setRegisterBtnActive(false);
            setNumberStatus('Please provide a valid 11 digit numbers');
        }
    },[mobileNo, networkGlobe, networkSmart])

    const passwordCheck = useCallback(() => {
        if (password === confirmPass) {
            setIsMatched(true);
            setRegisterBtnActive(true);
        } else {
            setIsMatched(false);
            setRegisterBtnActive(false);
        }
    }, [password,confirmPass])

    useEffect(() => {
       if (( firstName !== '' &&
        lastName !== '' &&
        middleName !== '' &&
        email !== '' &&
        password !== '' &&
        confirmPass !== '' &&
        mobileNo !== '' )) {
            passwordCheck();
            mobileNumberCheck();
        } else {
            if (password === confirmPass && password) {
                setIsMatched(true);
            } else {
                setIsMatched(false);
            }

            mobileNumberCheck();
            setRegisterBtnActive(false);
        }
    },[firstName, middleName, lastName, email, password, confirmPass, mobileNo, passwordCheck, mobileNumberCheck]);


    return(
        <div className="main registration-view">
            <Container>
                <h1 className="fredoka-font pb-4 pt-5 text-center">Create your account here!</h1>

                <Form onSubmit={(e) => handleSubmit(e)}>
                    <Row>
                        <Col>
                            <Form.Group controlId="firstName">
                                <Form.Label> First Name: </Form.Label>
                                <Form.Control type="text" value={firstName} onChange={event => setFirstName(event.target.value)} placeholder="Insert First Name Here" required/>
                            </Form.Group>
                        </Col>

                        <Col>
                            <Form.Group controlId="middleName">
                                <Form.Label> Middle Name: </Form.Label>
                                <Form.Control type="text" value={middleName} onChange={event => setMiddleName(event.target.value)} placeholder="Insert Middle Name Here" required/>
                            </Form.Group>
                        </Col>
                    </Row>
              
                    <Row>
                        <Col>
                            <Form.Group controlId="lastName">
                                <Form.Label> Last Name: </Form.Label>
                                <Form.Control type="text" value={lastName} onChange={event => setLastName(event.target.value)} placeholder="Insert Last Name Here" required/>
                            </Form.Group>
                        </Col>

                        <Col>
                            <Form.Group controlId="userEmail">
                                <Form.Label> Email Address: </Form.Label>
                                <Form.Control type="email" value={email} onChange={event => setEmail(event.target.value)} placeholder="Insert Email Here" required/>
                            </Form.Group>
                        </Col>
                    </Row>
                

            
                    <Row>
                        <Col>
                            <Form.Group controlId="password1">
                                <Form.Label> Password: </Form.Label>
                                <Form.Control type="password" value={password} onChange={event => setPassword(event.target.value)}  placeholder="Insert Password Here" required/>
                            </Form.Group>
                            {
                                isMatched ? 
                                    <p className="text-success">*Passwords matched*</p> : 
                                    <p className="text-danger">*Passwords should match*</p>
                                }
                        </Col>
                        
                        <Col>
                            <Form.Group controlId="password2">
                                <Form.Label> Confirm Password: </Form.Label>
                                <Form.Control type="password" value={confirmPass} onChange={event => setConfirmPass(event.target.value)} placeholder="Confirm Password Here" required/>
                            </Form.Group>
                        </Col>
                    </Row>
                 
                
                    <Form.Group controlId="mobileNumber">
                        <Form.Label> Mobile Number: </Form.Label>
                        <Form.Control type="tel" placeholder="Enter Mobile Number" onKeyPress={(event) => {if (!/[0-9]/.test(event.key)) {event.preventDefault();}}} onChange={event => setMobileNo(event.target.value)} value={mobileNo} maxLength={11} required/>
                    </Form.Group>
           
                    {
                        isNumberValid ? 
                        <p className="text-success">*{network} Network number is valid *</p> : 
                        <p className="text-danger">*{numberStatus}*</p>
                    }


                    {isRegisterBtnActive ? 
                    <Button type="submit" id="submitBtn" variant="warning" className="btn btn-block" >Create New Account</Button>
                    : 
                    <Button type="submit" id="submitBtn" variant="danger" className="btn btn-block" disabled>Create New Account</Button>  }
                    
                </Form>
            </Container>
        </div>
    )
}