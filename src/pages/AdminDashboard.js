import { Container, Row, Col, Tab, Nav, Form, Button,} from 'react-bootstrap';
import {useEffect ,useState} from 'react';
import ProductCard from "../components/ProductCard";
import Swal from 'sweetalert2';


export default function AdminDashboard() {

    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch("https://sheltered-mesa-34175.herokuapp.com/products/").then(fetchOutcome => fetchOutcome.json()).then(convertedData => {

            setProducts(convertedData.map(product => {
                return (
                    <ProductCard key={product._id} productInfo={product}/>
                )
            }))
        }) 
    },[]);


    
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [image, setImage] = useState('');
    const [ isBtnActive, setBtnActive] = useState('');   


    function handleSubmit(e) {
        e.preventDefault();

        fetch('https://sheltered-mesa-34175.herokuapp.com/products/create', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                images: image
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                Swal.fire({
                    title: `Product ${name} is created successfully!`,
                    icon: "success",
                    text: `product is now available`,
                });            
                
            } else {
                Swal.fire({
                    title: `User unauthorized `,
                    icon: "error",
                    text: `Account must be an admin to add a product`,
                });              
            }
        })
    }

    useEffect(() => {
        if (( name !== '' &&
         description !== '' &&
         price !== '' &&
         image !== ''  )) {
            setBtnActive(true);
         } else {
            setBtnActive(false);
         }
     },[name, description, price, image]);


    return (
        <div className="main admin-dashboard-view">
            <Container> 
                <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                    <Row>
                        <Col sm={3}>
                        <Nav variant="pills" className="flex-column">
                            <Nav.Item>
                            <Nav.Link eventKey="first">Show All Products</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                            <Nav.Link eventKey="second">Create New Product</Nav.Link>
                            </Nav.Item>
                        </Nav>
                        </Col>
                        <Col sm={9}>
                        <Tab.Content>
                            <Tab.Pane eventKey="first">
                                <h2 className="fredoka-font">Showing all products:</h2>
                                <div className="product-list admin-products">
                                    {products}
                                </div>
                            </Tab.Pane>
                            <Tab.Pane eventKey="second">
                                <h2 className="fredoka-font">Create Product:</h2>
                                <Container>
                                    <Form onSubmit={handleSubmit}>
                                        <Form.Group controlId="productTitle">
                                                <Form.Label> Product Title: </Form.Label>
                                            <Form.Control value={name} onChange={event => setName(event.target.value)} type="text" name="title" />
                                        </Form.Group>

                                        <Form.Group controlId="productDescription">
                                            <Form.Label> Product Description: </Form.Label>
                                            <Form.Control value={description} onChange={event => setDescription(event.target.value)} type="text" name="description" />
                                        </Form.Group>

                                        <Form.Group controlId="productDescription">
                                            <Form.Label> Product Price: </Form.Label>
                                            <Form.Control value={price} onChange={event => setPrice(event.target.value)} type="number" name="description" />
                                        </Form.Group>

                                        <Form.Group controlId="productDescription">
                                            <Form.Label>Image URL: </Form.Label>
                                            <Form.Control value={image} onChange={event => setImage(event.target.value)} type="text" name="description" />
                                        </Form.Group>

                                    {isBtnActive ? 
                                        <Button type="submit" id="submitBtn" variant="warning" className="btn btn-block" >Create Product</Button>
                                        : 
                                        <Button type="submit" id="submitBtn" variant="danger" className="btn btn-block" disabled>Create Product</Button>  }

                                </Form>
                                </Container>    
                            </Tab.Pane>
                        </Tab.Content>
                        </Col>
                    </Row>
                </Tab.Container>

            </Container>
        </div>
    )

}